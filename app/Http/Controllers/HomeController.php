<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\Models\Point;
use App\Models\Tweet;
use App\Models\TweetUser;
use App\Services\PointService;

class HomeController extends Controller
{
    /**
     * @var PointService
     */
    protected $pointService;

    public function __construct(PointService $pointService)
    {
        $this->pointService = $pointService;
    }

    public function getHome()
    {
        $firstLoadCount = 5000;

        $cacheKey = 'tweet-user-list-first';

        if (Cache::has($cacheKey) and app()->environment('production')) {
            $list = Cache::get($cacheKey);
        } else {
            $list = Point::orderBy('created_at')
                ->limit($firstLoadCount)
                ->select([
                    'title',
                    'body',
                    'lat',
                    'lng',
                ])
                ->get();
            if ($list->count() === $firstLoadCount) {
                Cache::put($cacheKey, $list, 60 * 10); // 10 minutes
            } else {
                Cache::put($cacheKey, $list, 60); // 1 minute
            }
        }

        $pointCount = $this->pointService->getPointCount();

        return view('home', [
            'list' => $list,
            'latestAt' => $list->count() ? $list->last()->created_at : null,
            'pointCount' => $pointCount,
        ]);
    }

    public function getComingSoon()
    {
        return view('coming-soon');
    }

    public function getTweet(Tweet $tweet)
    {
        $response = [
            'tweet_id' => $tweet['tweet_id'],
            'twitter_name' => $tweet['twitter_name'],
            'twitter_screen_name' => $tweet['twitter_screen_name'],
            'twitter_profile_image_url' => $tweet['twitter_profile_image_url'],
            'text' => $tweet['text'],
        ];

        return response()->json($response);
    }

    public function getTweetUser(int $id)
    {
        $user = TweetUser::findOrFail($id);
        $user->load('tweetsLess');

        return response()->json($user);
    }

    public function getLive(Request $request)
    {
        $request->validate([
            'latest_user_at' => 'required',
        ]);

        $cacheKey = 'tweet-user-list-' . str_replace(' ', '_', $request->input('latest_user_at'));

        if (Cache::has($cacheKey)) {
            $response = Cache::get($cacheKey);
        } else {
            $response = TweetUser::where('created_at', '>', $request->input('latest_user_at'))
                ->orderBy('created_at')
                ->limit(200)
                ->get();

            Cache::put($cacheKey, $response, 60); // 1 minute
        }

        return response()->json($response);
    }

    public function getSearchUser(Request $request)
    {
        $request->validate([
            'q' => 'min:2|max:100|required',
        ]);

        $query = $request->input('q');

        $list = TweetUser::where('twitter_name', 'like', '%' . $query . '%')
            ->orWhere('twitter_screen_name', 'like', '%' . $query . '%')
            ->limit(20)
            ->get();

        return response()->json($list);
    }

    public function add(Request $request)
    {
        if ($request->session()->has('added_point_id')) {
            return response()->json([
                'error' => 'You are already in the map.',
            ], 422);
        }

        $formData = $request->validate([
            'title' => 'required|min:3',
            'body' => 'required|min:5',
            'lat' => 'required',
            'lng' => 'required',
            'icon_id' => 'required',
        ]);

        $point = Point::create($formData);

        $request->session()->put('added_point_id', $point->id);
        $point->makeHidden([
            'id',
            'created_at',
            'updated_at',
        ]);

        return response($point);
    }

    public function getPoints(Request $request)
    {
        $formData = $request->validate([
            'north' => 'numeric|required',
            'south' => 'numeric|required',
            'west' => 'numeric|required',
            'east' => 'numeric|required',
        ]);

        $list = Point::orderBy('created_at')
            ->whereNotNull('approved_at')
            ->where('lng', '>', (float)$formData['west'])
            ->where('lng', '<', (float)$formData['east'])
            ->where('lat', '<', (float)$formData['north'])
            ->where('lat', '>', (float)$formData['south'])
            ->limit(10000)
            ->select([
                'title',
                'body',
                'lat',
                'lng',
                'icon_id',
            ])
            ->get();

        return response()->json($list);
    }
}
