<?php

namespace App\Http\Controllers\Badmin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

use App\Http\Controllers\Controller;
use App\Models\Point;

class PointController extends Controller
{
    public function index()
    {
        $list = Point::whereNull('approved_at')
            ->whereNull('rejected_at')
            ->orderBy('created_at', 'asc')
            ->orderBy('id', 'asc')
            ->paginate(100);

        return view('badmin.point.index', [
                'list' => $list,
            ]);
    }

    public function getApprovedList()
    {
        $list = Point::whereNotNull('approved_at')
            ->with('actionTakenBy')
            ->orderBy('approved_at', 'desc')
            ->orderBy('id', 'asc')
            ->paginate(100);

        return view('badmin.point.index-approved', [
                'list' => $list,
            ]);
    }

    public function getRejectedList()
    {
        $list = Point::whereNotNull('rejected_at')
            ->with('actionTakenBy')
            ->orderBy('rejected_at', 'desc')
            ->orderBy('id', 'asc')
            ->paginate(100);

        return view('badmin.point.index-rejected', [
                'list' => $list,
            ]);
    }

    public function update(int $pointId)
    {
        $point = Point::findOrFail($pointId);
        $point->rejected_at = null;
        $point->approved_at = now();
        $point->action_taken_by_id = auth()->id();
        $point->save();

        return redirect()->back()
            ->with([
                'success' => 'Approved',
            ]);
    }

    public function destroy(int $pointId)
    {
        $point = Point::findOrFail($pointId);
        $point->approved_at = null;
        $point->rejected_at = now();
        $point->action_taken_by_id = auth()->id();
        $point->save();

        return redirect()->back()
            ->with([
                'success' => 'Rejected',
            ]);
    }
}
