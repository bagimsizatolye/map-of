<?php

namespace App\Http\Controllers\Badmin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $matomoIframeURL = null;

        if (config('services.matomo.api_key')) {
            $matomoIframeURL = config('services.matomo.host');
            $matomoIframeURL .= 'index.php?module=Widgetize&action=iframe&moduleToWidgetize=Dashboard&actionToWidgetize=index&idSite=';
            $matomoIframeURL .= config('services.matomo.site_id');
            $matomoIframeURL .= '&period=week&date=yesterday&token_auth=';
            $matomoIframeURL .= config('services.matomo.api_key');
        }

        return view('badmin.home', [
            'matomoIframeURL' => $matomoIframeURL,
        ]);
    }
}
