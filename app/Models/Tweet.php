<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $fillable = [
        'tweet_id',
        'twitter_user_id',
        'twitter_name',
        'twitter_screen_name',
        'twitter_follow_count',
        'twitter_follower_count',
        'twitter_statuses_count',
        'twitter_profile_image_url',
        'sent_at',
        'text',
        'tweet_object',
        'top',
        'left',
    ];

    protected $casts = [
        'tweet_object' => 'array',
        'sent_at' => 'datetime',
    ];
}
