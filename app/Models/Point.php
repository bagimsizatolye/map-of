<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'body',
        'icon_id',
        'lng',
        'lat',
    ];

    protected $casts = [
        'approved_at' => 'timestamp',
        'rejected_at' => 'timestamp',
        'lat' => 'float',
        'lng' => 'float',
    ];

    public function actionTakenBy()
    {
        return $this->belongsTo(User::class, 'action_taken_by_id');
    }
}
