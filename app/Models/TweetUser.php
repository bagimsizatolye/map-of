<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TweetUser extends Model
{
    protected $fillable = [
        'id',
        'twitter_name',
        'twitter_screen_name',
        'twitter_follow_count',
        'twitter_follower_count',
        'twitter_statuses_count',
        'twitter_profile_image_url',
        'top',
        'left',
    ];

    protected $casts = [
        'id' => 'string',
    ];

    public function tweets()
    {
        return $this->hasMany(Tweet::class, 'twitter_user_id', 'id')
            ->orderBy('sent_at', 'asc');
    }

    public function tweetsLess()
    {
        return $this->hasMany(Tweet::class, 'twitter_user_id', 'id')
            ->select([
                'id', 'twitter_user_id', 'text', 'sent_at',
            ])
            ->orderBy('sent_at', 'asc');
    }
}
