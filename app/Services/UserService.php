<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Events\UserValidatedEvent;
use App\Model\Role;
use App\Models\User;
use Password;

class UserService
{
    public function getUser(int $id, array $relations = []): ?User
    {
        return User::with($relations)->find($id);
    }

    public function save(User $user, array $post): User
    {
        $action = 'insert';
        if ($user->id) {
            $action = 'update';
        }
        $user->name = $post['name'];

        if (!empty($post['email'])) {
            $user->email = $post['email'];
        }

        if (!empty($post['username'])) {
            $user->username = $post['username'];
        }

        if (!empty($post['password'])) {
            if ($action === 'insert' or $post['password'] !== $user->password) {
                $user->password = \Hash::make($post['password']);
            }
        }

        if (isset($post['phone'])) {
            $user->phone = $post['phone'];
        }

        if (isset($post['profession'])) {
            $user->profession = $post['profession'];
        }

        if (isset($post['gender'])) {
            $user->gender = $post['gender'];
        }

        if (isset($post['born_on'])) {
            $user->born_on = $post['born_on'];
        }

        if (isset($post['location1_id']) && $post['location1_id'] > 0) {
            $user->location1_id = $post['location1_id'];
        }

        if (isset($post['location2_id']) && $post['location2_id'] > 0) {
            $user->location2_id = $post['location2_id'];
        }

        $user->save();

        return $user;
    }

    public function getAdminList(array $options = []): LengthAwarePaginator
    {
        $userGroupNotIn = [
            'banned', //todo: const
        ];

        $query = User::query();

        // if (isset($options['filter'])) {
        //     if ($options['filter'] === 'notvalidated') {
        //         $query->whereNull('validated_at');
        //     }
        // } else {
        //     $query->whereNotNull('validated_at');
        // }

        return $query->paginate(30);
    }

    /**

     * @param $identifier
     *
     * @return \Illuminate\Database\Eloquent\Model|mixed|null|User
     */
    public function getUserByIdentifier($identifier): ?User
    {
        return User::where('email', $identifier)
            ->orWhere('username', $identifier)
            ->first();
    }

    public function sendPasswordReset(User $user, $token): void
    {
        \Mail::to($user)->send(new UserValidatedWelcomeMail($token, $user));
    }

    public function setUserPassword(User $user, string $password): User
    {
        $user->password = \Hash::make($password);
        $user->save();

        return $user;
    }
}
