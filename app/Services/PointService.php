<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Models\Point;

class PointService
{
    public function getPointCount(): int
    {
        return Point::count();
    }
}
