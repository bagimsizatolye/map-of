<?php

namespace App\Console\Commands;

use ErrorException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Thujohn\Twitter\Facades\Twitter;

use App\Models\Tweet;
use App\Models\TweetUser;

class FetchMissingTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gezi:fetch-missing {--since=} {--max=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = $this->option('since');
        $to = $this->option('max');

        $query = [
            'q' => '"' . config('services.twitter.query') . '" -filter:retweets',
            'result_type' => 'recent',
            'count' => 100,
            'include_entities' => true,
            'since_id' => $from,
        ];

        if (!empty($to)) {
            $query['max_id'] = $to;
        }

        $results = Twitter::getSearch($query);

        $addedCount = 0;

        foreach ($results->statuses as $status) {
            if (substr($status->text, 0, 3) === 'RT ') {
                continue; // is retweet
            }
            if (!Tweet::where('tweet_id', $status->id)->first()) {

                // Tweet::create([
                //     'tweet_id' => $status->id,
                //     'twitter_user_id' => $status->user->id,
                //     'retweeted_id' => isset($status->retweeted_status) ? $status->retweeted_status->id : null,
                //     'quoted_tweet_id' => isset($status->quoted_status_id) ? $status->quoted_status_id : null,
                //     'tweet_text' => $status->text,
                //     'sent_at' => Carbon::parse($status->created_at)->setTimezone('Europe/Istanbul'),
                //     'retweet_count' => $status->retweet_count,
                //     'favorite_count' => $status->favorite_count,
                // ]);

                $tweet = json_decode(json_encode($status), true);
                ;

                DB::beginTransaction();

                $imageUrl = $tweet['user']['profile_image_url_https'];

                try {
                    $imageContent = file_get_contents($imageUrl);
                    $fileName = strtolower(Str::random(20));
                    $path = storage_path('app/public/profile-images/' . $fileName . '.jpg');
                    \file_put_contents($path, $imageContent);
                    $imageUrl = '/storage/profile-images' . '/' . $fileName . '.jpg';
                } catch (Exception $e) {
                    dump($imageUrl, $fileName, $path);
                }

                $user = TweetUser::find($tweet['user']['id']);

                if (empty($user)) {
                    $user = TweetUser::create([
                        'id' => $tweet['user']['id'],
                        'twitter_name' => $tweet['user']['name'],
                        'twitter_screen_name' => $tweet['user']['screen_name'],
                        'twitter_follow_count' => $tweet['user']['friends_count'],
                        'twitter_follower_count' => $tweet['user']['followers_count'],
                        'twitter_statuses_count' => $tweet['user']['statuses_count'],
                        'twitter_profile_image_url' => $imageUrl,
                        'top' => rand(0, 95),
                        'left' => rand(0, 95),
                    ]);
                }

                $text = isset($tweet['extended_tweet']) ? $tweet['extended_tweet']['full_text'] : $tweet['text'];

                Tweet::create([
                    'tweet_id' => $tweet['id'],
                    'twitter_user_id' => $tweet['user']['id'],
                    'twitter_name' => $tweet['user']['name'],
                    'twitter_screen_name' => $tweet['user']['screen_name'],
                    'twitter_follow_count' => $tweet['user']['friends_count'],
                    'twitter_follower_count' => $tweet['user']['followers_count'],
                    'twitter_statuses_count' => $tweet['user']['statuses_count'],
                    'twitter_profile_image_url' => $imageUrl,
                    'text' => $text,
                    'tweet_object' => $tweet,
                    'sent_at' => $tweet['created_at'],
                    'top' => $user->top,
                    'left' => $user->left,
                ]);

                DB::commit();

                $addedCount++;
            }
        }
    }
}
