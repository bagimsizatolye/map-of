<?php

namespace App\Console\Commands;

use ErrorException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Spatie\TwitterStreamingApi\PublicStream;

use App\Models\Tweet;
use App\Models\TweetUser;

class ListenStream extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gezi:listen-stream';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PublicStream::create(
            config('services.twitter.access_token'),
            config('services.twitter.access_token_secret'),
            config('services.twitter.consumer_key'),
            config('services.twitter.consumer_key_secret')
        )->whenHears('#GezideyizBiz', function (array $tweet) {
            try {
                if ($tweet['retweeted'] === true) {
                    return true;
                }

                $text = isset($tweet['extended_tweet']) ? $tweet['extended_tweet']['full_text'] : $tweet['text'];
                if (substr($text, 0, 3) === 'RT ') {
                    return true;
                }

                $imageUrl = $tweet['user']['profile_image_url_https'];

                try {
                    $imageContent = file_get_contents($imageUrl);
                    $fileName = strtolower(Str::random(20));
                    $path = storage_path('app/public/profile-images/' . $fileName . '.jpg');
                    \file_put_contents($path, $imageContent);
                    $imageUrl = '/storage/profile-images' . '/' . $fileName . '.jpg';
                } catch (Exception $e) {
                    dump($imageUrl, $fileName, $path);
                }

                $user = TweetUser::find($tweet['user']['id_str']);

                if (empty($user)) {
                    $user = TweetUser::create([
                        'id' => $tweet['user']['id_str'],
                        'twitter_name' => $tweet['user']['name'],
                        'twitter_screen_name' => $tweet['user']['screen_name'],
                        'twitter_follow_count' => $tweet['user']['friends_count'],
                        'twitter_follower_count' => $tweet['user']['followers_count'],
                        'twitter_statuses_count' => $tweet['user']['statuses_count'],
                        'twitter_profile_image_url' => $imageUrl,
                        'top' => rand(0, 95),
                        'left' => rand(0, 95),
                    ]);
                }

                Tweet::create([
                    'tweet_id' => $tweet['id'],
                    'twitter_user_id' => $tweet['user']['id'],
                    'twitter_name' => $tweet['user']['name'],
                    'twitter_screen_name' => $tweet['user']['screen_name'],
                    'twitter_follow_count' => $tweet['user']['friends_count'],
                    'twitter_follower_count' => $tweet['user']['followers_count'],
                    'twitter_statuses_count' => $tweet['user']['statuses_count'],
                    'twitter_profile_image_url' => $imageUrl,
                    'text' => $text,
                    'tweet_object' => $tweet,
                    'sent_at' => $tweet['created_at'],
                    'top' => $user->top,
                    'left' => $user->left,
                ]);
            } catch (ErrorException $exception) {
                dump($tweet, $exception);
                // throw $exception;
            }
            echo "{$tweet['user']['screen_name']} who tweeted {$tweet['text']}";
        })->startListening();
    }
}
