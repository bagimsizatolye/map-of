<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Tweet;
use App\Models\TweetUser;

class CreateTweetUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gezi:create-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tweetList = Tweet::all();

        $bar = $this->output->createProgressBar(count($tweetList));
        $bar->start();

        foreach ($tweetList as $tweet) {
            if (empty($tweet->tweet_id)) {
                $tweet->tweet_id = $tweet->tweet_object['id'];
                $tweet->save();
            }

            $user = TweetUser::find($tweet->twitter_user_id);

            if (empty($user)) {
                TweetUser::create([
                    'id' => $tweet->twitter_user_id,
                    'twitter_name' => $tweet->twitter_name,
                    'twitter_screen_name' => $tweet->twitter_screen_name,
                    'twitter_follow_count' => $tweet->twitter_follow_count,
                    'twitter_follower_count' => $tweet->twitter_follower_count,
                    'twitter_statuses_count' => $tweet->twitter_statuses_count,
                    'twitter_profile_image_url' => $tweet->twitter_profile_image_url,
                    'top' => $tweet->top,
                    'left' => $tweet->left,
                ]);
            }

            $bar->advance();
        }

        $bar->finish();
    }
}
