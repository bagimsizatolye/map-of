<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use App\Models\Tweet;

class SaveRemoteProfileImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gezi:remote-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = Tweet::where('twitter_profile_image_url', 'like', '%twimg.com%')->get();

        $bar = $this->output->createProgressBar(count($list));
        $bar->start();

        foreach ($list as $tweet) {
            $imageContent = file_get_contents($tweet->twitter_profile_image_url);
            $fileName = strtolower(Str::random(20));
            $path = storage_path('app/public/profile-images/' . $fileName . '.jpg');
            \file_put_contents($path, $imageContent);
            $imageUrl = '/storage/profile-images' . '/' . $fileName . '.jpg';
            $tweet->twitter_profile_image_url = $imageUrl;
            $tweet->save();
            $bar->advance();
        }

        $bar->finish();
    }
}
