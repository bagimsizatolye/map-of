const mix = require('laravel-mix');

mix.disableSuccessNotifications();

mix.babelConfig({
  'presets': [
    [
      '@babel/preset-env',
      {
        'debug': true,
        'modules': false,
        'forceAllTransforms': true,
        'useBuiltIns': 'usage',
        'targets': 'last 1 version, > 1%, not IE 11, not IE_Mob 11',
      },
    ],
  ],
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const vueOptions = {
  compilerOptions: {
    whitespace: 'condense',
  },
  transpileOptions: {
    transforms: {
      'arrow': false,
      'classes': false,
      'computedProperty': false,
      'conciseMethodProperty': false,
      'dangerousForOf': false,
      'dangerousTaggedTemplateString': false,
      'defaultParameter': false,
      'destructuring': false,
      'forOf': false,
      'generator': false,
      'letConst': false,
      'modules': false,
      'numericLiteral': false,
      'parameterDestructuring': false,
      'reservedProperties': false,
      'spreadRest': false,
      'stickyRegExp': false,
      'templateString': false,
      'unicodeRegExp': false,
    },
  },
  // shadowMode: true,
};

mix.js('resources/js/app.js', 'public/js').vue({
  version: 2,
  options: vueOptions,
});
mix.sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/admin/admin.js', 'public/js').vue({
  version: 2,
  options: vueOptions,
});
mix.sass('resources/sass/admin/admin.scss', 'public/css');

if (mix.inProduction()) {
  mix.version();
} else {
  mix.webpackConfig({
    devtool: 'source-map',
  }).sourceMaps();
}
