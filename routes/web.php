<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@getComingSoon')->name('coming-soon');
Route::middleware(['throttle:global'])->group(function () {
    Route::get('/', [Controllers\HomeController::class, 'getHome'])
        ->name('home');
    // Route::get('tweet/{tweet:id}', [Controllers\HomeController::class, 'getTweet'])
    //     ->name('tweet.get');
    // Route::get('user/{id}', [Controllers\HomeController::class, 'getTweetUser'])
    //     ->name('tweet-user.get');
    // Route::get('live', [Controllers\HomeController::class, 'getLive'])
    //     ->name('live');
    // Route::get('search-user', [Controllers\HomeController::class, 'getSearchUser'])
    //     ->name('search.user');
    Route::post('add', [Controllers\HomeController::class, 'add'])
        ->middleware(['throttle:post-limit']);
    Route::get('points', [Controllers\HomeController::class, 'getPoints']);

    Route::middleware(['auth'])->prefix('badmin')->name('badmin.')->group(function () {
        Route::get('/', [Controllers\Badmin\AdminController::class, 'index'])
            ->name('home');
        Route::resource('user', Controllers\Badmin\UserController::class);
        Route::get('points/approved', [Controllers\Badmin\PointController::class, 'getApprovedList'])
            ->name('points-approved');
        Route::get('points/rejected', [Controllers\Badmin\PointController::class, 'getRejectedList'])
            ->name('points-rejected');
        Route::resource('points', Controllers\Badmin\PointController::class);
    });

    Auth::routes(['register' => false]);
});
