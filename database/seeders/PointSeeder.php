<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Point;

class PointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Point::factory()->count(5000)->create();
    }
}
