<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Point;
use App\Models\User;

class PointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Point::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $status = rand(1, 3);
        $user = User::find(1);

        return [
            'title' => $this->faker->words(rand(1, 3), true),
            'body' => $this->faker->sentences(rand(1, 3), true),
            'lat' => $this->faker->latitude,
            'lng' => $this->faker->longitude,
            'icon_id' => rand(1, 26),
            'approved_at' => $status === 2 ? now() : null,
            'rejected_at' => $status === 3 ? now() : null,
            'action_taken_by_id' => ($status !== 1 and !empty($user)) ? $user->id : null,
        ];
    }
}
