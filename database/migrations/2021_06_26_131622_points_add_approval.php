<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PointsAddApproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('points', function (Blueprint $table) {
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->bigInteger('action_taken_by_id')->unsigned()->nullable();
            $table->foreign('action_taken_by_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('points', function (Blueprint $table) {
            $table->dropForeign('points_action_taken_by_id_foreign');
        });

        Schema::table('points', function (Blueprint $table) {
            $table->dropColumn([
                'approved_at',
                'rejected_at',
                'action_taken_by_id',
            ]);
        });
    }
}
