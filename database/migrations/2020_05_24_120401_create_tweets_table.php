<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tweet_id')->unique();
            $table->bigInteger('twitter_user_id');
            $table->string('twitter_name', 250);
            $table->string('twitter_screen_name', 120);
            $table->integer('twitter_follow_count')->nullable();
            $table->integer('twitter_follower_count')->nullable();
            $table->integer('twitter_statuses_count')->nullable();
            $table->string('twitter_profile_image_url')->nullable();
            $table->string('text', 1000);
            $table->timestamp('sent_at')->index();
            $table->integer('top');
            $table->integer('left');
            $table->json('tweet_object');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
