<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTweetUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweet_users', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned();
            $table->string('twitter_name', 250);
            $table->string('twitter_screen_name', 120);
            $table->integer('twitter_follow_count')->nullable();
            $table->integer('twitter_follower_count')->nullable();
            $table->integer('twitter_statuses_count')->nullable();
            $table->string('twitter_profile_image_url')->nullable();
            $table->integer('top');
            $table->integer('left');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweet_users');
    }
}
