<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <base href="/">

  <title>{{ config('app.name', 'MapOf') }}</title>

  <!-- Scripts -->
  <script src="{{ mix('js/admin.js') }}" defer></script>

  <!-- Styles -->
  <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
  <!-- <link rel="icon" href="/assets/ui/images/favicon.ico" type="image/x-icon"> -->
</head>
<body>
  <div id="app">
    @auth
    <nav class="navbar-admin">
      <div class="p-4">
        <a href="{{ route('home') }}">
          <img src="images/united4istanbulconvention-logo.png" alt="Logo" class="img-fluid logo">
        </a>
      </div>
      <button class="hamburger hamburger--arrowturn d-lg-none" type="button" role="button">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
      <ul>
        <li class="{{ request()->routeIs('badmin.home') ? 'active' : '' }}">
          <a href="{{ route('badmin.home') }}">
            <i class="fab fa-lg fa-angellist"></i>
            {{ __('Dashboard') }}
          </a>
        </li>
        <li class="{{ request()->routeIs('badmin.points.index') ? 'active' : '' }}">
          <a href="{{ route('badmin.points.index') }}">
            <i class="fab fa-lg fa-angellist"></i>
            {{ __('Points') }}
          </a>
        </li>
        <li class="{{ request()->routeIs('badmin.points-approved') ? 'active' : '' }}">
          <a href="{{ route('badmin.points-approved') }}">
            <i class="fab fa-lg fa-angellist"></i>
            {{ __('Approved Points') }}
          </a>
        </li>
        <li class="{{ request()->routeIs('badmin.points-rejected') ? 'active' : '' }}">
          <a href="{{ route('badmin.points-rejected') }}">
            <i class="fab fa-lg fa-angellist"></i>
            {{ __('Rejected Points') }}
          </a>
        </li>
        <li class="{{ request()->routeIs('badmin.user.*') ? 'active' : '' }}">
          <a href="{{ route('badmin.user.index') }}">
            <i class="fas fa-user-alt fa-lg"></i>
            {{ __('Users') }}
          </a>
        </li>
      </ul>
    </nav>
    @endauth

    <main class="p-3">
      <header class="mb-3 px-3 px-lg-0">
        @yield('header')
      </header>

      @if(session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      @if(isset($errors) && $errors->count())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <ul>
            @foreach($errors->toArray() as $error)
              <li>{{{ $error[0] }}}</li>
            @endforeach
          </ul>
        </div>
      @endif

      @yield('content')
    </main>
  </div>
</body>
</html>
