@extends('layouts.admin')

@section('header')
  <h3>{{ config('app.name', 'Bağımsız Atölye') }} yönetici</h3>
@endsection

@section('content')
  @if(!empty($matomoIframeURL))
    <iframe
      src="{{ $matomoIframeURL }}"
      width="100%"
      height="100%"
      class="matomo-iframe"
    ></iframe>
  @else
    <script>
      window.location.href = '/badmin/points';
    </script>
  @endif
@endsection
