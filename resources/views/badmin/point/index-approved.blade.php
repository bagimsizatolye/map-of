@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>{{ __('Appoved Points') }}</h3>
    </div>
  </div>
@endsection

@section('content')
  <p>This table lists approved points. Latest approved first.</p>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Title') }}</th>
        <th>{{ __('Message') }}</th>
        <th>{{ __('Last Action By') }}</th>
        <th>{{ __('Actions') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>{{ $item->title }}</td>
          <td>{{ $item->body }}</td>
          <td>
            @if(!empty($item->actionTakenBy))
              {{ $item->actionTakenBy->name }}
            @endif
          </td>
          <td class="text-right">
            <form class="d-inline" action="{{ route('badmin.points.destroy', $item->id) }}" method="post">
              @csrf
              <input type="hidden" name="_method" value="delete">
              <button type="submit" class="btn btn-outline-danger btn-sm mb-1" data-ask="{{ __('Are you sure want to reject?') }}">
                {{ __('Reject') }}
              </button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $list->links() }}

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('There is no points waiting for approval.') }}
    </p>
  @endif
@endsection
