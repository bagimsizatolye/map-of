@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>{{ __('Rejected Points') }}</h3>
    </div>
  </div>
@endsection

@section('content')
  <p>This table lists rejected points. Latest rejected first.</p>

  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Title') }}</th>
        <th>{{ __('Message') }}</th>
        <th>{{ __('Last Action By') }}</th>
        <th>{{ __('Actions') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>{{ $item->title }}</td>
          <td>{{ $item->body }}</td>
          <td>
            @if(!empty($item->actionTakenBy))
              {{ $item->actionTakenBy->name }}
            @endif
          </td>
          <td class="text-right">
            <form class="d-inline" action="{{ route('badmin.points.update', $item->id) }}" method="post">
              @csrf
              <input type="hidden" name="_method" value="put">
              <button type="submit" class="btn btn-outline-success btn-sm mb-1">
                {{ __('Approve') }}
              </button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{ $list->links() }}

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('There is no points waiting for approval.') }}
    </p>
  @endif
@endsection
