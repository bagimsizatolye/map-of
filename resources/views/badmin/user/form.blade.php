@extends('layouts.admin')

@section('header')
  <h3>{{ $user->id ? $user->title : __('Add New User') }}</h3>
@endsection

@section('content')
  <form
    class="user-form"
    action="{{ $user->id ? route('badmin.user.update', $user->id) : route('badmin.user.store') }}"
    method="post"
    enctype="multipart/form-data"
  >
    @if ($user->id)
      <input type="hidden" name="_method" value="put">
    @endif

    @csrf

    <div class="form-group">
      <label for="name">{{ __('Name') }}</label>
      <input
        type="text"
        name="name"
        class="form-control"
        id="name"
        maxlength="255"
        minlength="5"
        required
        value="{{ old('name') ?: $user->name }}"
        aria-describedby="name-desc"
      >
      <small class="form-text text-muted" id="name-desc">
        Full name of user.
      </small>
    </div>

    <div class="form-group">
      <label for="email">{{ __('E-mail') }}</label>
      <input
        type="email"
        name="email"
        class="form-control"
        id="email"
        maxlength="255"
        minlength="5"
        required
        value="{{ old('email') ?: $user->email }}"
      >
    </div>

    <div class="form-group">
      <label for="password">{{ __('Password') }}</label>
      <input
        type="password"
        name="password"
        class="form-control"
        id="password"
        maxlength="255"
        minlength="8"
        value="{{ old('password') }}"
        aria-describedby="passwordDesc"
      >
      @if($user->id)
        <small class="form-text text-muted" id="passwordDesc">
          Enter the new password to change password.
        </small>
      @endif
    </div>

    <div class="text-right">
      <button type="submit" class="btn btn-outline-primary">{{ __('Save') }}</button>
    </div>

  </form>

@endsection
