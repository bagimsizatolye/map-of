@extends('layouts.admin')

@section('header')
  <div class="row">
    <div class="col-12 col-md-8">
      <h3>{{ __('Users') }}</h3>
    </div>
    <div class="col-12 col-md-4 text-xs-center text-md-right">
      <a class="btn btn-outline-dark btn-sm" href="{{ route('badmin.user.create') }}">
        {{ __('Add New User') }}
      </a>
    </div>
  </div>
@endsection

@section('content')
  <table class="table table-striped">
    <thead>
      <tr>
        <th>{{ __('Name') }}</th>
        <th>{{ __('E-mail') }}</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($list as $item)
        <tr>
          <td>{{ $item->name }}</td>
          <td>{{ $item->email }}</td>
          <td class="text-right">
              <a
                class="btn btn-outline-info btn-sm"
                href="{{ route('badmin.user.edit', [$item->id,]) }}"
              >{{ __('Edit') }}</a>
            <form class="d-inline" action="{{ route('badmin.user.destroy', $item->id) }}" method="post">
              @csrf
              <input type="hidden" name="_method" value="delete">
              <button type="submit" class="btn btn-outline-danger btn-sm" data-ask="{{ __('Are you sure want to delete this user?') }}">
                {{ __('Remove') }}
              </button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {!! $list->render() !!}

  @if($list->count() === 0)
    <p class="text-center">
      {{ __('No users.') }}
    </p>
  @endif
@endsection
