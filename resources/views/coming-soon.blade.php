<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ config('app.name') }}</title>

  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('css/coming-soon.css') }}">
  <style>
  </style>
</head>

<body>
  <div class="content">
    <h1>@lang("30 Mayıs'ı Bekleyin!")</h1>
  </div>
  <div class="social-icons">
    <a target="_blank" href="https://twitter.com/taksimdayanisma">
      <span class="ba-icon">E</span>
    </a>
    <a target="_blank" href="https://www.facebook.com/taksim.dynsm/">
      <span class="ba-icon">K</span>
    </a>
    <a target="_blank" href="https://instagram.com/gezideyizbiz">
      <span class="ba-icon">X</span>
    </a>
    <a target="_blank" href="https://www.youtube.com/channel/UCwdkg2sqvMGqyPy88FxSNdA">
      <span class="ba-icon">O</span>
    </a>
  </div>
  <div id="attribution">
    <a href="https://bagimsizatolye.org" target="_blank">Bağımsız Atölye</a> tarafından
    <a href="https://www.taksimdayanisma.org/" target="_blank">Taksim Dayanışması</a> için yapıldı.
    <!-- <a href="https://gitlab.com/bagimsizatolye/gezi-anniversary-map"><strong>&#127279;</strong></a> -->
  </div>
  @if(config('services.matomo.status'))
    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["setDomains", ["{{ config('services.matomo.domains') }}"]]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="{{ config('services.matomo.host') }}";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '{{ config('services.matomo.site_id') }}']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Matomo Code -->
    @endif
</body>
</html>
