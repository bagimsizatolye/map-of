<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>{{ config('app.name', 'MapOf') }}</title>
  <meta name="description" content="1 Temmuz’da karar geri çekilmezse Türkiye İstanbul Sözleşmesi’nden çıkacak. Kararı durdurmak için dünyanın her köşesinden her dilinden mesajlarımızla united4istanbulconvention.com’da buluşuyoruz! Haydi sen de avatarını seç mesajını yaz!">

  <meta name="twitter:card" content="summary">
  <meta name="twitter:creator" content="@bagimsizatolye">
  <meta name="twitter:site" content="@United4IC">
  <meta name="twitter:title" content="United4IstanbulConvention">

  <meta property="og:description" content="1 Temmuz’da karar geri çekilmezse Türkiye İstanbul Sözleşmesi’nden çıkacak. Kararı durdurmak için dünyanın her köşesinden her dilinden mesajlarımızla united4istanbulconvention.com’da buluşuyoruz! Haydi sen de avatarını seç mesajını yaz!">
  <meta name="twitter:description" content="1 Temmuz’da karar geri çekilmezse Türkiye İstanbul Sözleşmesi’nden çıkacak. Kararı durdurmak için dünyanın her köşesinden her dilinden mesajlarımızla united4istanbulconvention.com’da buluşuyoruz! Haydi sen de avatarını seç mesajını yaz!">

  <meta property="og:image" content="/images/united4ic-meta-image.jpg">
  <meta name="twitter:image:src" content="/images/united4ic-meta-image.jpg">

  <!-- Styles -->
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
  <div id="app"><map-of :point-count="{{ $pointCount }}"></map-of></div>
  <p id="attribution">
    Built by <a href="https://bagimsizatolye.org" target="_blank">Bağımsız Atölye</a>
    for <a href="https://united4istanbulconvention.medium.com/" target="_blank">United4IstanbulConvention</a>. <a href="https://gitlab.com/bagimsizatolye/map-of" target="_blank" title="Free &amp; Libre Software">{ <span style="transform: rotate(180deg); display: inline-block">&copy;</span> }</a>
  </p>
  <script async src="{{ mix('js/app.js') }}"></script>
  @if(config('services.matomo.status'))
    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(["setDomains", ["{{ config('services.matomo.domains') }}"]]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      _paq.push(['enableHeartBeatTimer']);
      (function() {
        var u="{{ config('services.matomo.host') }}";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '{{ config('services.matomo.site_id') }}']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <!-- End Matomo Code -->
    @endif
</body>
</html>
