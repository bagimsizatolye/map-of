import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Toast, { POSITION } from 'vue-toastification';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.use(VueAxios, axios);
Vue.use(Toast, {
  timeout: 10000,
  position: POSITION.TOP_RIGHT,
});

export const bus = new Vue();

export const app = new Vue({
  el: '#app',
  data() {
    return {};
  },
});

document.querySelectorAll('[data-ask]').forEach((item) => {
  item.onclick = () => {
    if (confirm(item.dataset.ask)) {
      return true;
    }

    return false;
  };
});


/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });
