export const TEXTS = [{
    id: 1,
    text: '#United4IstanbulConvention united4istanbulconvention.com The attempt to withdraw from the Istanbul Convention is unconstitutional, unlawful, and against international human rights norms.',
    lang: 'en',
  },
  {
    id: 2,
    text: '#United4IstanbulConvention united4istanbulconvention.com We are committed to realizing gender equality for all and we will persist until our demands for equality, non-discrimination and life free of violence are met.',
    lang: 'en',
  },
  {
    id: 3,
    text: '#United4IstanbulConvention united4istanbulconvention.com We are an international, intersectional united voice against global anti-gender attacks and will not back down in face of backlash.',
    lang: 'en',
  },
  {
    id: 4,
    text: '#United4IstanbulConvention united4istanbulconvention.com Regression from the Istanbul Convention should be recognized as a regression from commitment to equality, democracy and rule of law.',
    lang: 'en',
  },
  {
    id: 5,
    text: '#United4IstanbulConvention united4istanbulconvention.com We consider the withdrawal null and void and urge Turkey’s President to immediately reverse this decision.',
    lang: 'en',
  },
  {
    id: 6,
    text: '#United4IstanbulConvention united4istanbulconvention.com This withdrawal attempt is the culmination of global attacks against human rights in general, and women’s and LGBTI+ rights in particular. But we won’t back down.',
    lang: 'en',
  },
  {
    id: 7,
    text: '#İstanbulSözleşmesiİçinBirlikte united4istanbulconvention.com İstanbul Sözleşmesi’nden çekilme girişimi anayasaya ve uluslararası insan hakları normlarına aykırıdır, hukuksuzdur.',
    lang: 'tr',
  },
  {
    id: 8,
    text: '#İstanbulSözleşmesiİçinBirlikte united4istanbulconvention.com Kararlıyız, herkes için toplumsal cinsiyet eşitliği gerçekleşinceye, şiddetin ve ayrımcılığın olmadığı eşit bir yaşam talebimiz karşılanıncaya kadar mücadele edeceğiz.',
    lang: 'tr',
  },
  {
    id: 9,
    text: '#İstanbulSözleşmesiİçinBirlikte united4istanbulconvention.com Dünyadaki toplumsal cinsiyet karşıtı saldırılara karşı ulusötesi, kesişimsel bir  şekilde sesimizi birleştirdik, bu saldırılar karşısında geri adım atmayacağız.',
    lang: 'tr',
  },
  {
    id: 10,
    text: '#İstanbulSözleşmesiİçinBirlikte united4istanbulconvention.com İstanbul Sözleşmesi’nden geri adım atmak; eşitlik, demokrasi ve hukukun üstünlüğünden geri adım atmak demektir.',
    lang: 'tr',
  },
  {
    id: 11,
    text: '#İstanbulSözleşmesiİçinBirlikte united4istanbulconvention.com Sözleşmeden çekilme kararı yok hükmündedir. Cumhurbaşkanı, kararını bir an önce geri almalıdır.',
    lang: 'tr',
  },
  {
    id: 12,
    text: '#İstanbulSözleşmesiİçinBirlikte united4istanbulconvention.com Sözleşmeden çekilme girişimi, insan haklarına, özellikle de  kadın ve LGBTİ+ haklarına karşı küresel saldırıların doruk noktasıdır. Geri adım atmıyoruz!',
    lang: 'tr',
  },
];

export const ICONS = [{
    id: 1,
    url: '/images/marker-1.png',
    alt: 'Icon 1',
  },
  {
    id: 2,
    url: '/images/marker-2.png',
    alt: 'Icon 2',
  },
  {
    id: 3,
    url: '/images/marker-3.png',
    alt: 'Icon 3',
  },
  {
    id: 4,
    url: '/images/marker-4.png',
    alt: 'Icon 4',
  },
  {
    id: 5,
    url: '/images/marker-5.png',
    alt: 'Icon 5',
  },
  {
    id: 6,
    url: '/images/marker-6.png',
    alt: 'Icon 6',
  },
  {
    id: 7,
    url: '/images/marker-7.png',
    alt: 'Icon 7',
  },
  {
    id: 8,
    url: '/images/marker-8.png',
    alt: 'Icon 8',
  },
  {
    id: 9,
    url: '/images/marker-9.png',
    alt: 'Icon 9',
  },
  {
    id: 10,
    url: '/images/marker-10.png',
    alt: 'Icon 10',
  },
  {
    id: 11,
    url: '/images/marker-11.png',
    alt: 'Icon 11',
  },
  {
    id: 12,
    url: '/images/marker-12.png',
    alt: 'Icon 12',
  },
  {
    id: 13,
    url: '/images/marker-13.png',
    alt: 'Icon 13',
  },
  {
    id: 14,
    url: '/images/marker-14.png',
    alt: 'Icon 14',
  },
  {
    id: 15,
    url: '/images/marker-15.png',
    alt: 'Icon 15',
  },
  {
    id: 16,
    url: '/images/marker-16.png',
    alt: 'Icon 16',
  },
  {
    id: 17,
    url: '/images/marker-17.png',
    alt: 'Icon 17',
  },
  {
    id: 18,
    url: '/images/marker-18.png',
    alt: 'Icon 18',
  },
  {
    id: 19,
    url: '/images/marker-19.png',
    alt: 'Icon 19',
  },
  {
    id: 20,
    url: '/images/marker-20.png',
    alt: 'Icon 20',
  },
  {
    id: 21,
    url: '/images/marker-21.png',
    alt: 'Icon 21',
  },
  {
    id: 22,
    url: '/images/marker-22.png',
    alt: 'Icon 22',
  },
  {
    id: 23,
    url: '/images/marker-23.png',
    alt: 'Icon 23',
  },
  {
    id: 24,
    url: '/images/marker-24.png',
    alt: 'Icon 24',
  },
  {
    id: 25,
    url: '/images/marker-25.png',
    alt: 'Icon 25',
  },
  {
    id: 26,
    url: '/images/marker-26.png',
    alt: 'Icon 26',
  },
];
