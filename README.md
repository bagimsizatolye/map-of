## MapOf

Shows user entered points on map.

# Installing

Clone the repository. Set your environment variables like database credentials and twitter app credentials. Install PHP dependencies:

```
$ composer install
```

Then install frontend dependencies:

```
$ npm install
```

Run the database migrations:

```
$ php artisan migrate
```

Compile frontend assets:

```
$ npm run dev
```

If you would like to watch the frontend assets and compile on change, then run:

```
$ npm run watch
```

Now you're ready to run the application. Run PHP development server:

```
$ php artisan serve
```

You should run the twitter stream listener (this does not work right now):

```
$ php artisan gezi:listen-stream
```



<a href="https://bagimsizatolye.org" target="_blank">
<p align="center"><img src="https://bagimsizatolye.org/img/logo-dark.svg" width="400"></p>
</a>

## License

The project is open-sourced software licensed under the [GNU GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).
